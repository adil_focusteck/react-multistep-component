

### React Multistep Component Installation

Using npm
```
npm install react-multistep-component
```
### Usage

```js

import { Steps, Step } from 'react-multistep-component';

{
  /*
   * <Steps/> it's the container and wrapper for your steps.
   *
   * `currentStep` is the selected step when first render. By default the first (1) step will
   * be selected. *optional
   *
   * `stepShouldChange` is called whenever a step is changed. This method can be used for
   * validations. By default will return `true`. *optional
   *
   * `onStepChange` is called after step change (include the first time when render) and have
   * the current step how attribute
   *
   * `prevButton`/`nextButton` is a wrapper for the buttons, `html`, `jsx` or `string` can be included.
   * ex: `prevButton={<span><img src="..."/>Step 1</span>}`. *optional
   *
   * `mountOnlySiblings` if it's set `true`, only the siblings of the currently active  step will be
   * render. This is an improvement for big implementations.
   *
   */
}
<Steps currentStep={2}>
  {
    /*
     * <Step/> it's the wrapper for your step content. All content inside this will be tranclude.
     *
     * `customNavigator` `html`, `jsx` or `string` that will be used as a label of the step. *optional
     *
     */
  }
  <Step>
    Example Step 1
    <span>Hello step 1</span>
  <Step/>
  <Step>
    <div>
      Javascript Rocks!
    </div>
  <Step/>
</Steps>

```

### Styles

By default the component doesn't contain styles.

## Todo

- Add `redux` example
- Add key events
- Add more examples

### Licence
MIT
